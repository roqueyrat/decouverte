# Les aspirateurs

## Sommaire:
1. Histoire des aspirateurs
2. Points positifs et négatifs des aspirateurs
3. Aspirateurs sans fil vs aspirateurs avec fil
4. Mathématiques dans un aspirateur  
    a) Formule mathématiques de l'aspiration  
    b) Code informatique dans un aspirateur

### I) Histoire des aspirateurs

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Un **aspirateur**, aussi appelé balayeuse au Canada (les deux termes y sont employés indistinctement), est un appareil électroménager muni d’une pompe à air créant une dépression qui provoque l’aspiration de poussière et de petits déchets tombés au sol.
Ce type d’outil sert au nettoyage des moquettes et des tapis, ainsi que des parquets et autres surfaces.
Il a été inventé 1860 par **Daniel Hess** pour remplacer son ancêtre, le balais à tapis.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Le premier système de nettoyage utilisant le vide pour aspirer la poussière était manuel.
Il s'agit du *Whirlwind*, inventé à Chicago en 1869 par **Ives W. McGaffey**.
Cette machine légère et compacte était toutefois difficile à utiliser car il fallait tourner une manivelle tout en la poussant sur le sol.
L'aspirateur motorisé a été inventé en 1901 par **Hubert Cecil Booth**, un ingénieur britannique.
Celui-ci avait remarqué que le nettoyage des sièges de trains par époussetage serait avantageusement remplacé par un système qui aspirerait la poussière au lieu de la faire s'envoler ailleurs.
Après avoir essayé lui-même d'aspirer la poussière d'un des sièges à travers un mouchoir, il a mis en pratique le procédé dans un appareil connu sous le nom de *Puffing Billy*.
En 1905, le *Griffith's Improved Vacuum Apparatus for Removing Dust from Carpets* est breveté par **Walter Griffiths**.
Il s'agit d'un nouveau système manuel facilement transportable, facile à ranger et qui peut être utilisé par une seule personne.
L'opérateur doit presser un système de soufflet qui aspire la poussière via un tuyau flexible et amovible auquel plusieurs embouts peuvent être attachés.
Il s'agit du premier aspirateur domestique comparable aux aspirateurs actuels.

### II) Points positifs et négatifs des aspirateurs

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comme toutes inventions, les aspirateurs ont leurs qualités mais aussi leurs défauts.
Tout d'abord, voici les différentes qualités des aspirateurs:
* prise en main simple
* aspiration généralement assez puissante
* assez peu encombrant
* facilement accessible dans les magasins électroménagers

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Et maintenant, voici les différents défauts des aspirateurs:
* coût peut être élevé pour les meilleurs aspirateurs
* aspirateurs avec fil assez lourd
* bruyant
* long à utiliser pour nettoyer une grande surface

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Malgré ces points négatifs, les aspirateurs sont de plus en plus utilisés de nos jours pour remplacer les balais.
Ce sont certainement les sacs d'aspirateur (qui permettent de stocker la saleté aspirée) qui est à l'origine de ce phénomène car en effet, le balai est beaucoup plus léger, maniable et moins coûteux que l'aspirateur.

### III) Aspirateurs sans fil vs aspirateurs avec fil

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actuellement, il existe 2 types d'aspirateurs: les aspirateurs avec et sans fil.
Dans cette partie, nous étudierons les 2 modèles pour en déduire lequel est le meilleur.
Pour cela, nous comparerons les 2 types dans le tableau suivant:

| Aspirateurs  | Autonomie   | Masse  | Prix moyen  |
| ------- | -------- | ------- | -------- |
| Aspirateur sans fil   |  15 min à 1 heure  |  3 kg  |  350 euros  |
| Aspirateur avec fil   |      Infinie       |  5 à 8 kg  |  200 euros  |

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En conclusion de ce tableau, nous ne pouvons pas déterminer quel type d'aspirateur est le meilleur car ils possèdent chacun leurs qualités et leurs défauts.

### IV) Mathématiques dans un aspirateur

#### a) Formule mathématiques de l'aspiration

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pour calculer la puissance d'aspiration d'un aspirateur, on calcule le air watt. 
Le air watt est une mesure de la performance de l’aspirateur résultant du produit du débit et de la dépression de l’air.
Ainsi, cette mesure est très importante, mais elle dépend de biens d’autres facteurs.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La formule mathématiques pour calculer le air watt est la suivante:

$\sum_{n=1}^{10} n^2$

**airwatts** = $\frac{mmCE \times débit}{101,84}$